import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.infinitegraph.*;
import com.infinitegraph.indexing.*;
import com.infinitegraph.navigation.*;
import com.infinitegraph.navigation.qualifiers.*;

import java.io.*;
import java.net.*;

public class CallGraphApp {

	/** The name of the graph database. */
	private static String dbName = "CallGraph";
	
	/** The properties filepath. */
	private static String propertiesFile = "config/CallGraph.properties";
	
	private static Logger logger = null;

	private static class CustomGuide implements Guide
	{
		private String kind;
		private boolean start;

		public CustomGuide(String kind)
		{
			this.kind = kind;
			this.start = true;
		}

		@Override
		public Strategy getHopOrder(VertexHandle currentVertex, List<Hop> order)
		{
			if (start)
			{
				for (ListIterator<Hop> iter = order.listIterator(order.size()); iter.hasPrevious();)
				{
					Hop h = (Hop) iter.previous();
					if(h.hasEdge()) {
						FuncFuncEdge e = (FuncFuncEdge) h.getEdge();
						if(e.getKind() != null && e.getKind().equals(kind) == false) {
							iter.remove();
						}
							
					}
				}
				start = false;
			}

			// use a depth first approach in our guide
			return Guide.Strategy.DEPTH_FIRST;
		}
	};

	static class CallQ implements Qualifier {
		CallQ() {
		}
		
		@Override
		public boolean qualify(Path p) {
			FuncFuncEdge e;		
			Function v;
			int idx = 0;		
			if(p.size() > 5)
				return false;
			for(Hop h: p) {
				idx++;
				if(h.hasEdge()) {
					e = (FuncFuncEdge) h.getEdge();
					v = (Function) h.getVertex();
					if(idx > 2 && e.getKind() == null)
						return false;
					if(e.getKind() != null && e.getKind().equals("called-by"))
					    return false;
				}
			}
			return true;
		}
	};

	static class PrintPathResultsHandler implements NavigationResultHandler 
	{
	    private Logger logger = null;
		private PrintWriter out = null;
	    PrintPathResultsHandler(PrintWriter out)
	    {
			logger = LoggerFactory.getLogger(PrintPathResultsHandler.class);
			this.out = out;
	    }
	
	    public void handleResultPath(Path result, Navigator navigator) {
			//logger.info("FOUND MATCHING PATH: ");
			String path = result.get(0).getVertex().toString();
			// For h in p
	        for(Hop h : result)
	        {
	            if(h.hasEdge())
	            {
	            	path = path + "," + h.getVertex().toString();
	            } 
	        }
	        //logger.info("{}", path);
			out.println(path);
		}
	}

	static class ClientConn implements Runnable {
		private Socket socket;
		private GraphDatabase g;
		private Transaction t;
		private static GraphIndex<Function> funcIndex = null;

		ClientConn(Socket s) {
			socket = s;
			try {
				g = GraphFactory.open(dbName, propertiesFile);
			} catch (Exception e) {
				System.out.println("ClientConn() bad");
				e.printStackTrace();
			}
		}

		public void run() {
			BufferedReader in = null;
			PrintWriter out = null;
			try {
				in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
				out = new PrintWriter(socket.getOutputStream(), true);
			} catch(IOException e) {
				e.printStackTrace();
				return;
			}
			
			String msg;
			t = g.beginTransaction(AccessMode.READ);
			try {
				funcIndex = IndexManager.<Function>createGraphIndex(Function.class.getName(), "function");
				while((msg = in.readLine()) != null) {
					if(msg.startsWith("path:")) {
						msg = msg.substring(5);
						// navigate
						Vertex start = (Vertex)g.getNamedVertex(msg);
						if(start == null) {
							continue;
						}
						PrintPathResultsHandler resultPrinter = new PrintPathResultsHandler(out);
						CallQ q = new CallQ();
						Navigator myNavigator = start.navigate(new CustomGuide("calls"), q, Qualifier.ANY, resultPrinter);
						myNavigator.start();
						myNavigator.stop();
						out.println("-1");
					} else {
						List<Function> existing = funcIndex.lookup(new PredicateQuery("function =~ \""+msg+"\""), 20);
						if(existing.size() > 0) {
							out.println("" + existing.size());
							ListIterator<Function> it = existing.listIterator();
							while(it.hasNext()) {
								Function f = it.next();
								out.println(f.getFunction());
							}
						} else {
							out.println("0");
						}						
					}
				}
			} catch(Exception e) {
				e.printStackTrace();
			} finally {
				try {
					socket.close();
				} catch(Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	

	public static void main(String[] args) {
		try{
			
			logger = LoggerFactory.getLogger(CallGraph.class);
			
			//Open the GraphDatabase 
			//GraphDatabase myGraph = GraphFactory.open(dbName, propertiesFile);
			//Transaction transaction = myGraph.beginTransaction(AccessMode.READ_WRITE);

			//Start a TCP server
			ServerSocket server = null;
			server = new ServerSocket(9999);
			
			Socket client = null;
			while(true) {
				client = server.accept();
				Thread t = new Thread(new ClientConn(client));
				t.start();
			}
			//Close the Graph database
		} 
		catch (Exception e) {
			logger.error("Exception in main: {}" , e.getMessage());
			e.printStackTrace();
		}
	}


	
}