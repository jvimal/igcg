import com.infinitegraph.BaseEdge;


public class FuncFuncEdge extends BaseEdge {
	/* calls or called-by */
	private String type;

	public FuncFuncEdge(String t) {
		markModified();
		type = t;
	}

	public String getKind() {
		return type;
	}

	@Override public String toString(){
		fetch();
		return type;
	}
}
