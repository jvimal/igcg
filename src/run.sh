#!/bin/sh
if [ `uname` = "Linux" ]; then
classes=`echo ~/codeparse/ig/linux86_64/lib/*.jar | sed 's/ /:/g'`:src/
else
classes=`echo ~/Desktop/ig/mac86_64/lib/*.jar | sed 's/ /:/g'`:src/
fi

if [ ! -z "$1" ]; then
  java -cp $classes CreateDb
  java -cp $classes CallGraph $1
fi

java -cp $classes CallGraphApp

