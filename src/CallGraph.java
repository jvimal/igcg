import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.infinitegraph.*;
import com.infinitegraph.indexing.*;
import com.infinitegraph.navigation.*;
import com.infinitegraph.navigation.qualifiers.*;

import java.io.*;

public class CallGraph {

	/** The name of the graph database. */
	private static String dbName = "CallGraph";
	
	/** The properties filepath. */
	private static String propertiesFile = "config/CallGraph.properties";
	
	/** Index references. */
	private static GraphIndex<Function> funcIndex =null;
	
	private static Logger logger = null;
	

	public static void main(String[] args) {
		try{
			
			logger = LoggerFactory.getLogger(CallGraph.class);
			
			//Open the GraphDatabase 
			GraphDatabase myGraph = GraphFactory.open(dbName, propertiesFile);
			
			//Ingest data into the database
			doIngest(myGraph, args[0]);
			
			logger.info("\n---------------------------\n");
			
			//Lookup some nodes and edges and print to screen.
			//printGraph(myGraph);
			
			//Close the Graph database
		} 
		catch (Exception e) {
			logger.error("Exception in main: {}" , e.getMessage());
			e.printStackTrace();
		}
	}


	public static void doIngest(GraphDatabase myGraph, String filename) throws IndexException
	{
		logger.info("Doing ingest");
		BufferedReader in;
		try {
			in = new BufferedReader(new FileReader(filename));
		} catch(Exception e) {
			System.out.println("File " + filename + " not found!");
			return;
		}
		String line = "";
		//Begin an update transaction
		Transaction transaction = myGraph.beginTransaction(AccessMode.READ_WRITE);	
		try{
			//Set up automatic indexing (alternately could use Lucene Indexing)
			funcIndex = IndexManager.<Function>createGraphIndex(Function.class.getName(), "function");
			transaction.commit();
		
			transaction = myGraph.beginTransaction(AccessMode.READ_WRITE);
			Function ffrom = null, fto = null;
			int skip = 0;
			while((line = in.readLine()) != null) {
				if(line.length() == 0)
					continue;
				if(skip > 0) {
					skip -= 1;
					continue;
				}
				if(line.charAt(0) == ' ') {
					String data[] = line.substring(4).split(" ");
					fto = createOrGet(myGraph, data[0], "");
					FuncFuncEdge e1 = new FuncFuncEdge("calls");
					FuncFuncEdge e2 = new FuncFuncEdge("called-by");
					myGraph.addEdge(e1, ffrom, fto, EdgeKind.OUTGOING);
					//myGraph.addEdge(e2, fto, ffrom, EdgeKind.BIDIRECTIONAL);
					System.out.println("Adding edge " + ffrom.getFunction() + " <==> " + fto.getFunction());
				} else {
					String data[] = line.split(" ", 2);
					//System.out.println(data[0] + " ==> " + data[1]);
					ffrom = createOrGet(myGraph, data[0], data[1]);
				}
				if(line.indexOf("<={{") != -1) {
					skip = 1;
				}
			}
			transaction.commit();
			logger.info("Finished Ingest.");
			in.close();
		}
		catch (Exception e){
			logger.error("Exception in doIngest: {}" , e.getMessage());
			logger.error(line);
			e.printStackTrace();
			//transaction.rollback();		
			transaction.commit();
		}
		finally{
			logger.info("Complete transaction");
			transaction.complete();
		}	
	}

	public static Function createOrGet(GraphDatabase g, String fname, String loc) throws IndexException {
		Function f;
		fname = fname.replaceAll(Pattern.quote("()"), "");
		if((f = (Function)g.getNamedVertex(fname)) == null) {
			f = new Function(fname, loc);
			g.addVertex(f);
			g.nameVertex(f.getFunction(), f);
		}
		return f;
	}
};