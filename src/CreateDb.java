import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.infinitegraph.*;;

/**
 * Main class for the URL tagger example.
 */
public class CreateDb {

	/** The name of the graph database. */
	private static String dbName = "CallGraph";

	/** The properties filepath. */
	private static String propertiesFile = "config/CreateDb.properties";

	private static Logger logger = null;
	
	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args)
	{
		logger = LoggerFactory.getLogger(CreateDb.class);
		try
		{
			try
			{
				// Remove any existing DBs
				GraphFactory.delete(dbName, propertiesFile);
			} 
			catch (StorageException e)
			{
				logger.warn(e.getMessage());
			} 	
			//	create new graph database
			GraphFactory.create(dbName, propertiesFile);
		} 
		catch (StorageException e)
		{
			logger.error(e.getMessage());
			e.printStackTrace();
			return;
		} 
		catch (ConfigurationException e)
		{
			logger.error(e.getMessage());
			e.printStackTrace();
			return;
		} 
		finally
		{
			logger.info("created new graph database");
		}
	}
}
