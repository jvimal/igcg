import com.infinitegraph.BaseVertex;
import java.util.regex.Pattern;

public class Function extends BaseVertex {
	private String function;
	private String location;
	
	public Function(String f, String l) {
		markModified();
		function = f.replaceAll(Pattern.quote("()"), "");
		location = l;
	}

	public String getFunction(){
		fetch();
		return function;
	}
	
	public String getLocation(){
		fetch();
		return location;
	}
	
	public String setLocation(String loc) {
		markModified();
		location = loc;
		return location;
	}
	
	@Override public String toString(){
		fetch();
		return function;
	}
}
