#!/bin/sh
if [ `uname` = "Linux" ]; then
classes=`echo ~/codeparse/ig/linux86_64/lib/*.jar | sed 's/ /:/g'`:src/
else
classes=`echo ~/Desktop/ig/mac86_64/lib/*.jar | sed 's/ /:/g'`:src/
fi

javac -cp $classes src/CreateDb.java
javac -cp $classes src/CallGraphApp.java
javac -cp $classes src/CallGraph.java


#java -cp $classes CreateDb
#java -cp $classes CallGraph rawdata/sample.txt

