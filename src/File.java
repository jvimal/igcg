import com.infinitegraph.BaseVertex;

public class File extends BaseVertex {
	private String path;
	
	public File(String p) {
		markModified();
		path = p;
	}

	public String getPath(){
		fetch();
		return path;
	}
	
	@Override public String toString(){
		fetch();
		return path;
	}
}
